package com.me.demo.web;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PageAndSortRequest {

    private int page = 0;
    private int size = 10;
    private List<String> order = new ArrayList<>();
    private List<String> sortBy = new ArrayList<>();

    public PageRequest getPageRequest() {
        return new PageRequest(page, size, getSort());
    }

    public Sort getSort() {
        Sort sort = null;
        if (!CollectionUtils.isEmpty(sortBy)) {
            List<Sort.Order> orders = new ArrayList<>();
            int idx = 0;
            for (String sortBy : this.sortBy) {
                orders.add(new Sort.Order(getDirection(idx), sortBy));
                idx++;
            }
            sort = new Sort(orders);
        }
        return sort;
    }

    public Sort.Direction getDirection(int idx) {
        if (order != null && order.size() > idx) {
            String direction = order.get(idx);
            return Sort.Direction.valueOf(direction.toUpperCase());
        }
        return order.stream().findFirst().map(x -> Sort.Direction.valueOf(x.toUpperCase())).orElse(Sort.DEFAULT_DIRECTION);
    }

}
