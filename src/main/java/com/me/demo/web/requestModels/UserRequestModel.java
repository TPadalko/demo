package com.me.demo.web.requestModels;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
public class UserRequestModel implements Serializable {

    @NotNull
    private String email;
    @NotNull
    private String password;
    private String login;
}
