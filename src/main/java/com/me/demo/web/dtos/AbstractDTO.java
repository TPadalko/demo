package com.me.demo.web.dtos;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AbstractDTO implements Serializable {
    private Long id;
}
