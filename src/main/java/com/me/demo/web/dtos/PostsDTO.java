package com.me.demo.web.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
public class PostsDTO extends AbstractDTO {

    private String text;
    private LocalDateTime creationDate;
    private String author;
}
