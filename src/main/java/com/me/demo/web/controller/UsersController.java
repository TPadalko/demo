package com.me.demo.web.controller;

import com.me.demo.exceptions.DemoException;
import com.me.demo.model.UsersEntity;
import com.me.demo.service.AbstractService;
import com.me.demo.service.UsersService;
import com.me.demo.web.dtos.UserDTO;
import com.me.demo.web.requestModels.UserRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/users")
public class UsersController extends AbstractController<UsersEntity, Long> {

    @Autowired
    private UsersService usersService;

    @PermitAll
    @PostMapping("/create")
    public ResponseEntity<Void> createTrader(@RequestBody @Valid UserRequestModel request) throws DemoException {
        usersService.createUser(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all")
    public ResponseEntity<List<UserDTO>> findAllUsers() {
        List<UsersEntity> allUsers = usersService.findAll();
        List<UserDTO> dtos = allUsers.stream().map(user -> new UserDTO(user.getUsername())).collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @Override
    protected AbstractService<UsersEntity, Long> getService() {
        return usersService;
    }
}
