package com.me.demo.web.controller;

import com.me.demo.model.PostsEntity;
import com.me.demo.service.AbstractService;
import com.me.demo.service.PostsService;
import com.me.demo.web.dtos.PostsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("api/posts")
public class PostsController extends AbstractController<PostsEntity, Long> {

    @Autowired
    private PostsService postsService;

    @GetMapping("/all")
    public ResponseEntity<List<PostsDTO>> findAllUsers(@RequestParam (required = false) String text,
                                                       @RequestParam (required = false) LocalDateTime dateFrom,
                                                       @RequestParam (required = false) LocalDateTime dateTo,
                                                       @RequestParam (required = false) Long userId) {

        if(dateFrom == null) dateFrom = LocalDateTime.MIN;
        if(dateTo == null) dateTo = LocalDateTime.now();
        return new ResponseEntity<>(postsService.filter(text, dateFrom, dateTo, userId), HttpStatus.OK);
    }


    @Override
    protected AbstractService<PostsEntity, Long> getService() {
        return postsService;
    }
}
