package com.me.demo.web.controller;

import com.me.demo.service.AbstractService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.data.domain.Persistable;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;

@Slf4j
public abstract class AbstractController<E extends Persistable<ID>, ID extends Serializable>  {
    protected abstract AbstractService<E, ID> getService();

    @GetMapping
    public List<E> findAll() {
        return getService().findAll();
    }

    @GetMapping("/byId/{id}")
    public E findOne(@PathVariable ID id) {
        E entity = getService().findOne(id);
        if (entity == null) {
            throw new EntityNotFoundException();
        }
        return entity;
    }

    @PostMapping
    public E create(@Valid @RequestBody E entity) {
        return getService().save(entity);
    }

    @PutMapping
    public E update(@Valid @RequestBody E entity) {
        if (!getService().exists(entity.getId())) {
            throw new EntityNotFoundException();
        }
        return getService().save(entity);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable ID id, HttpServletResponse response) {
        try {
            if (!getService().exists(id)) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            getService().delete(id);
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } catch (DataIntegrityViolationException exception) {
            // Unable to delete due to integrity constraint violation. Expected.
            log.warn(MessageFormat.format("Unable to delete entity with ID: {0} due to integrity violation.", id), exception);
            response.setStatus(HttpServletResponse.SC_CONFLICT);
        } catch (DataRetrievalFailureException exception) {
            // Unable to find entity.
            log.warn(MessageFormat.format("Unable to delete entity with ID: {0} due to retrieval exception.", id), exception);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } catch (Exception exception) {
            // Other exception. Might be a lock failure, second delete may work (hopefully).
            log.error(MessageFormat.format("Unable to delete entity with ID: {0}.", id), exception);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
