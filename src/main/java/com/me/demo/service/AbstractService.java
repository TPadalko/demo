package com.me.demo.service;

import com.me.demo.web.PageAndSortRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface AbstractService<E extends Persistable<ID>, ID extends Serializable> {

    E save(E entity);

    List<E> save(Collection<E> entities);

    void delete(E entity);

    void delete(Collection<E> entities);

    void delete(ID id);

    E findOne(ID id);

    List<E> findAll(Collection<ID> ids);

    List<E> findAll();

    long count();

    boolean exists(ID id);

    Page<E> findAll(PageAndSortRequest pageAndSort);

    E getReference(Class<E> clazz, ID id);

    List<E> getReferences(Class<E> clazz, Collection<ID> ids);

}
