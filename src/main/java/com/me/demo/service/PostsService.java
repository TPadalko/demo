package com.me.demo.service;

import com.me.demo.model.PostsEntity;
import com.me.demo.web.dtos.PostsDTO;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public interface PostsService extends AbstractService<PostsEntity, Long> {

    List<PostsEntity> fidAllForUser(Long userId);

    List<PostsDTO> filter(String text, LocalDateTime dateFrom, LocalDateTime dateTo, Long userId);
}
