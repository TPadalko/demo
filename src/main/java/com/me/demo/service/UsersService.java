package com.me.demo.service;

import com.me.demo.exceptions.DemoException;
import com.me.demo.model.UsersEntity;
import com.me.demo.web.requestModels.UserRequestModel;


public interface UsersService extends AbstractService<UsersEntity, Long>  {

    void createUser(UserRequestModel requestModel) throws DemoException;

    UsersEntity findByEmail(String email);

    UsersEntity findLoggedUser();
}
