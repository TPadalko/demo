package com.me.demo.service.impl;


import com.me.demo.model.PostsEntity;
import com.me.demo.repository.PostsRepository;
import com.me.demo.service.PostsService;
import com.me.demo.web.dtos.PostsDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class PostsServiceImpl extends AbstractServiceImpl<PostsEntity, Long> implements PostsService {

    @Autowired
    private PostsRepository postsRepository;

    @Override
    @Transactional(readOnly = true)
    public List<PostsEntity> fidAllForUser(Long userId) {
        return postsRepository.findAllByCreator(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PostsDTO> filter(String text, LocalDateTime dateFrom, LocalDateTime dateTo, Long userId) {
        log.debug("filter() text: " + text);
        log.debug("filter() dateFrom: " + dateFrom);
        log.debug("filter() dateTo: " + dateTo);
        log.debug("filter() userId: " + userId);

        List<PostsDTO> result = new ArrayList<>();

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT p FROM POSTS p WHERE ");
        if(text != null) {
            sql.append("p.text LIKE :text AND ");
        }
        sql.append("p.creation_date BETWEEN :dateFrom AND :dateTo ");
        if(userId != null) {
            sql.append("AND p.creator_id = :userId");
        }

        Query query = entityManager.createNativeQuery(sql.toString())
                .setParameter("text", prepareText(text))
                .setParameter("dateFrom", dateFrom)
                .setParameter("dateTo", dateTo)
                .setParameter("userId", userId);

        List<PostsEntity> postsEntities = query.getResultList();
        if(postsEntities != null) {
            result = postsEntities.stream().map(entity -> new PostsDTO(entity.getText(), entity.getCreationDate(),
                    entity.getCreator().getLogin())).collect(Collectors.toList());
        }
        return result;
    }

    private String prepareText(String text) {
        return "'%" + text + "%'";
    }

    @Override
    protected JpaRepository<PostsEntity, Long> getRepo() {
        return postsRepository;
    }
}
