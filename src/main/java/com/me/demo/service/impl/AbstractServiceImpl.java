package com.me.demo.service.impl;

import com.me.demo.service.AbstractService;
import com.me.demo.web.PageAndSortRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractServiceImpl<E extends Persistable<ID>, ID extends Serializable> implements AbstractService<E, ID> {

    @PersistenceContext
    protected EntityManager entityManager;

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected abstract JpaRepository<E, ID> getRepo();

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public E save(final E entity) {
        return getRepo().save(entity);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<E> save(final Collection<E> entities) {
        return getRepo().save(entities);
    }

    @Transactional(rollbackFor = Throwable.class)
    public List<E> saveAll(final Collection<E> entities) {
        return getRepo().save(entities);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void delete(final E entity) {
        getRepo().delete(entity);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void delete(final Collection<E> entities) {
        getRepo().delete(entities);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void delete(final ID id) {
        delete(getRepo().findOne(id));
    }

    @Override
    @Transactional(readOnly = true)
    public E findOne(final ID id) {
        return getRepo().findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<E> findAll(final Collection<ID> ids) {
        return getRepo().findAll(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<E> findAll() {
        return getRepo().findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public long count() {
        return getRepo().count();
    }

    @Override
    @Transactional(readOnly = true)
    public boolean exists(ID id) {
        return getRepo().exists(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<E> findAll(PageAndSortRequest pageAndSort) {
        return getRepo().findAll(pageAndSort.getPageRequest());
    }

    @Override
    public E getReference(Class<E> clazz, ID id) {
        return entityManager.getReference(clazz, id);
    }

    @Override
    public List<E> getReferences(Class<E> clazz, Collection<ID> ids) {
        return ids.stream().map(id -> this.getReference(clazz, id)).collect(Collectors.toList());
    }
}