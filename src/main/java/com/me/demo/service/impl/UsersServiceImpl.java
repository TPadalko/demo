package com.me.demo.service.impl;

import com.me.demo.enums.Role;
import com.me.demo.exceptions.DemoException;
import com.me.demo.exceptions.ExceptionHelper;
import com.me.demo.exceptions.UserAlreadyExists;
import com.me.demo.model.UsersEntity;
import com.me.demo.repository.UsersRepository;
import com.me.demo.service.UsersService;
import com.me.demo.web.requestModels.UserRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service("usersService")
public class UsersServiceImpl extends AbstractServiceImpl<UsersEntity, Long> implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    @Transactional
    public void createUser(UserRequestModel requestModel) throws DemoException {
        UsersEntity user = usersRepository.findByEmail(requestModel.getEmail());
        if(user != null) throw ExceptionHelper.newException(UserAlreadyExists.class, requestModel.getEmail());

        user = new UsersEntity();
        user.setEmail(requestModel.getEmail());
        user.setRole(Role.USER);
        user.setLogin(requestModel.getLogin());
        user.setCreationDate(LocalDateTime.now());
        //TODO hash password before saving to database
        user.setPassword(requestModel.getPassword());
        usersRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public UsersEntity findByEmail(String email) {
        return usersRepository.findByEmail(email);
    }

    @Override
    @Transactional(readOnly = true)
    public UsersEntity findLoggedUser() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        String email = null;
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                email = springSecurityUser.getUsername();
            } else if (authentication.getPrincipal() instanceof String) {
                email = (String) authentication.getPrincipal();
            }
        }
        return usersRepository.findByEmail(email);
    }

    @Override
    protected JpaRepository<UsersEntity, Long> getRepo() {
        return usersRepository;
    }
}
