package com.me.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.domain.Persistable;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
public abstract class AbstractEntity<ID extends Serializable> implements Persistable<ID> {
    private static final long serialVersionUID = 812186236495289464L;

    @Override
    @JsonIgnore
    public boolean isNew() {
        return getId() == null;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("rawtypes")
        final Persistable that = (Persistable) obj;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public String toString() {
        return getId().toString();
    }
}
