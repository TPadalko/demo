package com.me.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "POSTS")
@Getter
@Setter
public class PostsEntity extends AbstractLongEntity {
    private static final long serialVersionUID = 1L;

    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    private UsersEntity creator;
}
