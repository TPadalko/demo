package com.me.demo.exceptions;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
public abstract class ExceptionHelper {

    public static DemoException newException(Class<? extends DemoException> exceptionClass, String... errorParams) {
        try {
            DemoException exceptionTmp = exceptionClass.newInstance();
            Field msgField = exceptionClass.getDeclaredField("msg");
            msgField.setAccessible(true);
            String message = (String) msgField.get(exceptionTmp);

            if (errorParams != null) {
                message = MessageFormat.format(message, errorParams);
            }

            DemoException demoException = exceptionClass.getConstructor(String.class).newInstance(message);
            demoException.setErrorParams(Arrays.stream(errorParams).collect(Collectors.toList()));
            return demoException;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return new DemoException("Internal server error");
    }
}
