package com.me.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.FORBIDDEN)
public class UserAlreadyExists extends DemoException {

    private final String msg = "User with email {0} already exists";

    public UserAlreadyExists(String message) {
        super(message);
    }
}
