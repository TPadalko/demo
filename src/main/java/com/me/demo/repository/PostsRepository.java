package com.me.demo.repository;

import com.me.demo.model.PostsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostsRepository extends JpaSearchRepository<PostsEntity, Long> {

    @Query("select p from PostsEntity p where p.creator.id = :userId")
    List<PostsEntity> findAllByCreator(@Param("userId") Long userId);
}
