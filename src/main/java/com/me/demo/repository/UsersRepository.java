package com.me.demo.repository;

import com.me.demo.model.UsersEntity;
import org.springframework.stereotype.Repository;


@Repository
public interface UsersRepository extends JpaSearchRepository<UsersEntity, Long> {

    UsersEntity findByLogin(String login);

    UsersEntity findByEmail(String email);
}
